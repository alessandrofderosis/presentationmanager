package presentationmanager.exceptions;

public class EmptyHostsCollection extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1960911769341273199L;

	public EmptyHostsCollection() {
		super("Empty Hosts Collection");
	}
}
