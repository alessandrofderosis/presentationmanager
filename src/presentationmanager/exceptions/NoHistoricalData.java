package presentationmanager.exceptions;

public class NoHistoricalData extends Exception {

	private static final long serialVersionUID = -8135207554356534069L;

	public NoHistoricalData() {
		super("No historical data");
	}
}
