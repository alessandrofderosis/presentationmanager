package presentationmanager.exceptions;

public class NoHostID extends Exception {
	private static final long serialVersionUID = 697147872807923040L;

	public NoHostID() {
		super("Id not present");
	}
}
