package presentationmanager.view;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import presentationmanager.exceptions.EmptyHostsCollection;
import presentationmanager.exceptions.NoHistoricalData;
import presentationmanager.exceptions.NoHostID;
import presentationmanager.logic.AbstractPresenterManager;
import presentationmanager.logic.Host;
import presentationmanager.logic.PresenterManager;

public class MainConsole {
	private static final String DATE_PATTERN = "dd-MM-yyyy HH:mm:ss.SSS";

	public static void main(String[] args) {

		AbstractPresenterManager presenterManager = new PresenterManager();

		SimpleDateFormat frmt = new SimpleDateFormat(DATE_PATTERN);

		System.out.println("Welcome to the Presentation Manager App");
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

		while (true) {
			printMenu();
			int option;
			try {
				option = Integer.parseInt(in.readLine());

				switch (option) {
				case 1:
					handleInsertion(presenterManager, in);
					break;
				case 2:
					handleRemoval(presenterManager, in);
					break;
				case 3:
					handleRandomSelection(presenterManager, in);
					break;
				case 4:
					handleHistoricalView(presenterManager, frmt, in);
					break;
				case 9:
					return;
				default:
					System.out.println("Please follow the instruction for the input.");
					break;
				}
			} catch (IOException | NumberFormatException | ParseException e) {
				System.out.println("Please follow the instruction for the input.");
				continue;
			} catch (NoHostID e) {
				System.out.println("Be sure that the id selected is in the list");
				continue;
			} catch (EmptyHostsCollection e) {
				System.out.println("No host");
				continue;
			} catch (NoHistoricalData e) {
				System.out.println("No historical data");
				continue;
			}
		}
	}
	
	private static void printMenu() {
		System.out.println();
		System.out.println("Press 1 to add an host");
		System.out.println("Press 2 to remove an host");
		System.out.println("Press 3 to choose a random host");
		System.out.println("Press 4 to display the excluded hosts in a particular date");
		System.out.println("Press 9 to exit");
	}

	private static void handleHistoricalView(AbstractPresenterManager presenterManager, SimpleDateFormat frmt,
			BufferedReader in) throws NoHistoricalData, IOException, ParseException {
		
		System.out.println("Presentations List:");
		presenterManager.getPresentationsDates().forEach(d -> System.out.println(frmt.format(d)));
		System.out.println("Please insert the presentation date in which you are interested, following the "
				+ DATE_PATTERN + " pattern");
		String dateString = in.readLine();
		Date date = frmt.parse(dateString);
		Collection<Host> excludedHostsByDate = presenterManager.getExcludedHostsByDate(date);
		if (excludedHostsByDate.isEmpty())
			System.out.println("No excluded host");
		else
			excludedHostsByDate.forEach(h -> System.out.println(h));
	}

	private static void handleRandomSelection(AbstractPresenterManager presenterManager, BufferedReader in)
			throws EmptyHostsCollection, IOException {
		presenterManager.getHosts().forEach(h -> System.out.println(h));
		System.out.println("Please insert the ids of the hosts not available, separated by space:");
		String line = in.readLine();
		String[] split = line.split("\\s+");
		List<Integer> excludedIDS = new ArrayList<>();
		if (!line.isEmpty())
			excludedIDS = Arrays.asList(split).stream().mapToInt(Integer::parseInt).boxed()
					.collect(Collectors.toList());
		Date date = new Date(System.currentTimeMillis());
		System.out.println("The selected host is: ");
		System.out.println(presenterManager.getRandomHost(date, excludedIDS));
		System.out.println();
	}

	private static void handleRemoval(AbstractPresenterManager presenterManager, BufferedReader in)
			throws EmptyHostsCollection, IOException, NoHostID {
		presenterManager.getHosts().forEach(h -> System.out.println(h));
		System.out.println("Please insert the id of the host to remove:");
		int id = Integer.parseInt(in.readLine());
		presenterManager.removeHost(id);
	}

	private static void handleInsertion(AbstractPresenterManager presenterManager, BufferedReader in)
			throws IOException {
		System.out.println("Please insert the name of the host: ");
		String name = in.readLine();
		presenterManager.addHost(new Host(name));
	}

}
