package presentationmanager.logic;

public class Host {

	private String name;
	private int id;
	private static int counter = 0;

	public Host(String name) {
		this.id = counter;
		this.name = name;
		counter++;
	}

	public String getName() {
		return name;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Host other = (Host) obj;
		if (id != other.id)
			return false;
		return true;
	}

	public int getId() {
		return id;
	}

	@Override
	public String toString() {
		return "Host [name=" + name + ", id=" + id + "]";
	}
	

	
}
