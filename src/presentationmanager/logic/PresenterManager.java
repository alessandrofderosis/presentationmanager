package presentationmanager.logic;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.TreeMap;

import presentationmanager.exceptions.EmptyHostsCollection;
import presentationmanager.exceptions.NoHistoricalData;
import presentationmanager.exceptions.NoHostID;

public class PresenterManager extends AbstractPresenterManager {
	private Map<Integer, Host> hosts_by_id;
	private Map<Date, Set<Host>> excluded_hosts_by_date;

	public PresenterManager() {
		hosts_by_id = new HashMap<>();
		excluded_hosts_by_date = new TreeMap<>();
	}

	@Override
	public Collection<Host> getHosts() throws EmptyHostsCollection {
		if (hosts_by_id.isEmpty())
			throw new EmptyHostsCollection();
		return Collections.unmodifiableCollection(hosts_by_id.values());
	}

	@Override
	public void addHost(Host h) {
		hosts_by_id.put(h.getId(), h);

	}

	@Override
	public void removeHost(int id) throws NoHostID {
		if (hosts_by_id.remove(id) == null)
			throw new NoHostID();
	}

	@Override
	public Host getRandomHost(Date d, List<Integer> excludedIDS) throws EmptyHostsCollection {
		if (hosts_by_id.isEmpty())
			throw new EmptyHostsCollection();

		Set<Host> excludedHosts = new HashSet<>();
		for (Integer id : excludedIDS) {
			Host host = hosts_by_id.get(id);
			if (host != null)
				excludedHosts.add(host);
		}
		excluded_hosts_by_date.put(d, excludedHosts);
		
		
		
		HashSet<Integer> possiblePresenters = new HashSet<Integer>(hosts_by_id.keySet());
		possiblePresenters.removeAll(excludedIDS);

		if (possiblePresenters.isEmpty())
			throw new EmptyHostsCollection();

		Random random = new Random();
		int indexToReturn = random.nextInt(possiblePresenters.size());
		int c = 0;
		for (Integer id : possiblePresenters) {
			if (c == indexToReturn)
				return hosts_by_id.get(id);
			c++;
		}
		return null;
	}

	@Override
	public Collection<Host> getExcludedHostsByDate(Date d) {
		return Collections.unmodifiableCollection(excluded_hosts_by_date.get(d));
	}

	@Override
	public Collection<Date> getPresentationsDates() throws NoHistoricalData {
		if (excluded_hosts_by_date.isEmpty())
			throw new NoHistoricalData();
		return Collections.unmodifiableCollection(excluded_hosts_by_date.keySet());
	}

}
