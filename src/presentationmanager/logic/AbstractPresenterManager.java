package presentationmanager.logic;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import presentationmanager.exceptions.EmptyHostsCollection;
import presentationmanager.exceptions.NoHistoricalData;
import presentationmanager.exceptions.NoHostID;

public abstract class AbstractPresenterManager {

	public abstract Collection<Host> getHosts() throws EmptyHostsCollection;
	
	public abstract void addHost(Host h);
	
	public abstract void removeHost(int id) throws NoHostID;
	
	public abstract Host getRandomHost(Date d, List<Integer> excluded) throws EmptyHostsCollection;
	
	public abstract  Collection<Host> getExcludedHostsByDate(Date d);
	
	public abstract Collection<Date> getPresentationsDates() throws NoHistoricalData;

}
